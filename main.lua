-------------------------------------------------------------------------------------

display.setStatusBar( display.HiddenStatusBar )

local physics = require("physics")
local gameUI = require("gameUI")
local easingx = require("easingx")

local ongoing = false
local GAMESTARTED = false
local playerIn = 4

local FRANK_BULLETS = true

local blueLife = 5
local redLife = 5
local purpleLife = 5
local greenLife = 5

local blueX = 0
local blueY = 0

local redX = display.contentWidth
local redY = display.contentHeight

local purpleX = 0
local purpleY = display.contentHeight

local greenX = display.contentWidth
local greenY = 0

local blueCollisionFilter   = { categoryBits = 1, maskBits = 16}--224 } --16
local greenCollisionFilter  = { categoryBits = 2, maskBits = 32}--208 } --32
local redCollisionFilter    = { categoryBits = 4, maskBits = 64}-- 176 } --64
local purpleCollisionFilter = { categoryBits = 8, maskBits = 128} --112} --128

local blueEnemyFilter   = { categoryBits = 16, maskBits = 1}--14 } --1
local greenEnemyFilter  = { categoryBits = 32, maskBits = 2}--13 } --2
local redEnemyFilter    = { categoryBits = 64, maskBits = 4}--11 } --4
local purpleEnemyFilter = { categoryBits = 128, maskBits = 8}--7}--8

physics.start()
physics.setGravity( 0, 0 ) -- no gravity in any direction

bangSound = audio.loadSound ("bang.wav")

system.activate( "multitouch" )

local enemyGfx = { "rocket_blue.png", "rocket_green.png", "rocket_red.png", "rocket_purple.png" }
local allVerticalEnemies = {} -- empty table for storing objects
local allHorizontalEnemies = {}
local allEnemies = {}

local allPlayers = {}

local blueScore = display.newText("", 64, 32, nil, 50)
blueScore.xScale = 1.2;	blueScore.yScale = 1.2;	blueScore.rotation = 90

local redScore = display.newText("", display.contentWidth-64, display.contentHeight-100, nil, 50)
redScore.xScale = 1.2;	redScore.yScale = 1.2;	redScore.rotation = 270
	
local greenScore = display.newText("", display.contentWidth-64, 32, nil, 50)
greenScore.xScale = 1.2; greenScore.yScale = 1.2; greenScore.rotation = 270

local purpleScore = display.newText("", 64, display.contentHeight-100, nil, 50)
purpleScore.xScale = 1.2;	purpleScore.yScale = 1.2;	purpleScore.rotation = 90

-- Automatic culling of offscreen objects
local function removeOffscreenItems()
	
	for i = 1, #allEnemies do
		local oneEnemy = allEnemies[i]
		if (oneEnemy and oneEnemy.x) then
			if oneEnemy.x < -100 or oneEnemy.x > display.contentWidth + 100 or oneEnemy.y < -100 or oneEnemy.y > display.contentHeight + 100 then
				oneEnemy:removeSelf()
                table.remove( allEnemies, i ) 
 			end	
		end
	end

end

local function removeOffscreenDilara()
	
	for i = 1, #allVerticalEnemies do
		local oneEnemy = allVerticalEnemies[i]
		if (oneEnemy and oneEnemy.x) then
			if oneEnemy.x < -100 or oneEnemy.x > display.contentWidth + 100 or oneEnemy.y < -100 or oneEnemy.y > display.contentHeight + 100 then
				oneEnemy:removeSelf()
                table.remove( allVerticalEnemies, i ) 
 			end	
		end
	end
	
	for i = 1, #allHorizontalEnemies do
		local oneEnemy = allHorizontalEnemies[i]
		if (oneEnemy and oneEnemy.x) then
			if oneEnemy.x < -100 or oneEnemy.x > display.contentWidth + 100 or oneEnemy.y < -100 or oneEnemy.y > display.contentHeight + 100 then
				oneEnemy:removeSelf()
                table.remove( allHorizontalEnemies, i ) 
 			end	
		end
	end
end


local function spawnVerticalEnemies()
	num = math.random(0, 60)
	if (num % 17 == 1) then  

		enemyRand = math.random(1,4)
		if enemyRand == 1 then
			flt = blueEnemyFilter	
		elseif enemyRand == 2 then
			flt = greenEnemyFilter
		elseif enemyRand == 3 then
			flt = redEnemyFilter
		else
			flt = purpleEnemyFilter
		
		end
		
		randImage = enemyGfx[ enemyRand ]

		allVerticalEnemies[#allVerticalEnemies + 1] = display.newImage( randImage )
		local enemy = allVerticalEnemies[#allVerticalEnemies]
		
		enemy.x = math.random(140,display.contentWidth-140)
		enemy.y = 0 --math.random(0,display.contentHeight)
		enemy.rotation = 90
		--RANDOM X (0-contentwidth)
		--RANDOM Y ( minus values)
		physics.addBody( enemy, { density=1.0, friction=1, bounce=0 , filter= flt})
		enemy:setLinearVelocity( 0, 100 )
		enemy.myName = "enemy"
				enemy.isBullet = true

	end
end

local function spawnHorizontalEnemies()
--Choose colors 

--randomly select Y position between 0 to contentheight
	num = math.random(0, 60)
	if (num % 17 == 1) then  

		enemyRand = math.random(1,4)
		if enemyRand == 1 then
			flt = blueEnemyFilter	
		elseif enemyRand == 2 then
			flt = greenEnemyFilter
		elseif enemyRand == 3 then
			flt = redEnemyFilter
		else
			flt = purpleEnemyFilter
		
		end
		
		randImage = enemyGfx[ enemyRand ]
		
		allHorizontalEnemies[#allHorizontalEnemies + 1] = display.newImage( randImage )
		local enemy = allHorizontalEnemies[#allHorizontalEnemies]
		
		enemy.y = math.random(140,display.contentHeight-140)
		enemy.x = 0 --math.random(0,display.contentHeight)
	
		physics.addBody( enemy, { density=1.0, friction=1, bounce=0 , filter= flt})
		enemy:setLinearVelocity( 100, 0 )
		enemy.myName = "enemy"
		enemy.isBullet = true

	end

end

local function startDrag( event )
	
	local t = event.target
	local phase = event.phase
	local stage = display.getCurrentStage()
	
	local phase = event.phase
	if "began" == phase then
	
		if playerIn < 4 then
			playerIn = playerIn+1
		end
	
		stage:setFocus( t, event.id )
		t.isFocus = true

		-- Store initial position
		t.x0 = event.x - t.x
		t.y0 = event.y - t.y
		
		-- Make body type temporarily "kinematic" (to avoid gravitional forces)
		--event.target.bodyType = "kinematic"
		
		-- Stop current motion, if any
		t:setLinearVelocity( 0, 0 )
		t.angularVelocity = 0

	elseif t.isFocus then
		
			if "moved" == phase then
				t.x = event.x - t.x0
				t.y = event.y - t.y0

			elseif "ended" == phase or "cancelled" == phase then
				stage:setFocus( nil )
				t.isFocus = false
				-- Stop current motion, if any
				t:setLinearVelocity( 0, 0 )
				t.angularVelocity = 0
				
			end
	end

	-- Stop further propagation of touch event!
	return true
end

local function initializePlayers()

	local blue_player = display.newImage("fingermark_blue.png", 128,128);
	blue_player.x = blueX
	blue_player.y = blueY
	blue_player.life = blueLife
	blue_player.score = blueScore
	allPlayers[#allPlayers + 1] = blue_player
	
	local purple_player = display.newImage("fingermark_purple.png", 128,128);
	purple_player.x = purpleX
	purple_player.y = purpleY
	purple_player.life = purpleLife
	purple_player.score = purpleScore
	allPlayers[#allPlayers + 1] = purple_player
	
	local green_player = display.newImage("fingermark_green.png", 128,128);
	green_player.x = greenX
	green_player.y = greenY
	green_player.life = greenLife
	green_player.score = greenScore
	allPlayers[#allPlayers + 1] = green_player
	
	local red_player = display.newImage("fingermark_red.png", 128,128);
	red_player.x = redX
	red_player.y = redY
	red_player.life = redLife
	red_player.score = redScore
	allPlayers[#allPlayers + 1] = red_player
	
	physics.addBody(blue_player,{ density=1.0, friction=1, bounce=0, filter = blueCollisionFilter} )
	physics.addBody(red_player,{ density=1.0, friction=1, bounce=0, filter =redCollisionFilter } )
	physics.addBody(purple_player,{ density=1.0, friction=1, bounce=0, filter =purpleCollisionFilter } )
	physics.addBody(green_player,{ density=1.0, friction=1, bounce=0, filter =greenCollisionFilter } )
	
	red_player.bodyType = "kinematic"
	blue_player.bodyType = "kinematic"
	purple_player.bodyType = "kinematic"
	green_player.bodyType = "kinematic"
	
	blue_player:addEventListener( "touch", startDrag ) -- make object draggable
	purple_player:addEventListener( "touch", startDrag ) -- make object draggable
	red_player:addEventListener( "touch", startDrag ) -- make object draggable
	green_player:addEventListener( "touch", startDrag ) -- make object draggable
	
	green_player.myName = "green_player"	
	blue_player.myName = "blue_player"
	purple_player.myName = "purple_player"
	red_player.myName = "red_player"

end

local function onCollision( event )

   if ( event.phase == "began" ) then

      --print( "began: " .. event.object1.myName .. " and " .. event.object2.myName )
	  audio.play( bangSound )
	  
			  -- Collision event
		local function delay()
			-- Change the body's type to static
			if event.object1.life > 0 then 
				event.object1.life = event.object1.life -1
			end
			
			local explode = display.newImage("bang.png")
			explode.x = event.object2.x; explode.y = event.object2.y
			explode.xScale = 0.8; explode.yScale = 0.8
		
			transition.to(explode, { time = 500, xScale = 1.5, yScale = 1.5, transition = easingx.easeOut }) -- "pop" animation
			transition.to( explode, { time=500, delay=500, alpha=0.0 })
			
			--event.object2:removeSelf()
			--event.object2 = nil
			
			event.object2.x = -100
			event.object2.y = -100
			
			--print (event.object1.myName .. " " .. event.object1.life)
			
			if event.object1.life <= 0 then
				event.object1.x = -100
				event.object1.y = -100
				display.getCurrentStage():setFocus( nil )
				event.object1.isFocus = false
				--if event.object1 ~= nil then 
					--event.object1.isFocus = false
					--print(event.object1)
						--event.object1:removeSelf()
				--end
			
			end
			
		end

		timer.performWithDelay( 100, delay )

	end
end
local function undoOngoing()
	ongoing = false
	--print("feel good")
	--removeOffscreenItems()
	end
local lastColor1
local lastColor2
local newSpawn = true
local randomSpawnDirection
local randomSpawn
local randomFail1
local randomFail2
local currentSpawnNumber =0
local randomColor
local angle = 0.0;
	local myXPos = 0;
	local myYPos = 0;
	local xFocused = false;
	local xPos = 0;
	local yPos = 0;
	local xVel, yVel 
	local ScreenHeight = display.contentHeight;
	local ScreenWidth = display.contentWidth;
	 
local function enemies()
	
	if newSpawn then 
	
	newSpawn = false
	randomSpawnDirection = math.random(0,3);
	randomSpawn = math.random(0,1);
	
	currentSpawnNumber = 0;
		randomColor = math.random(0,3);
	while randomColor == lastColor1 or randomColor == lastColor2 do
		randomColor = math.random(0,3);
	end
	lastColor1 = lastColor2;
	lastColor2 = randomColor;
	
	randImage = enemyGfx[randomColor+1]

	if randomColor == 0 then
		randImage = enemyGfx[1] --BLUE
		flt = blueEnemyFilter
	elseif randomColor == 1 then
		randImage = enemyGfx[2] -- gre
		flt = greenEnemyFilter
	elseif randomColor == 2 then
		randImage = enemyGfx[3] -- red
		flt = redEnemyFilter
	elseif randomColor == 3 then
		randImage = enemyGfx[4] -- purp
		flt = purpleEnemyFilter
	end

	if randomSpawnDirection == 0 then
		xFocused = false;
		xPos = -50;
		angle = 0;
		xVel = 100;
		yVel = 0;
	elseif randomSpawnDirection == 1 then
		xFocused = true;
		yPos = ScreenHeight + 50;
		angle = -90;
		xVel = 0;
		yVel = -100;
	elseif randomSpawnDirection == 2 then
		xFocused = false;
		xPos = ScreenWidth+50;
		angle = 180;
		xVel = -100;
		yVel = 0;
	elseif randomSpawnDirection == 3 then
		xFocused = true;
		yPos = -50;
		angle = 90; 
		xVel = 0;
		yVel = 100;
	else
		--	Error
	end
	if xFocused then
		randomFail1 = math.random(1,5);
		
		if math.random(0,1) == 0 then
			randomFail2 = math.random(1,5);
			while randomFail2 ==randomFail1 do
				randomFail2 = math.random(1,5);
			end
		else
			if math.random(0,1) == 0 then
				randomFail2 = randomFail1 - 1
			else
				randomFail2 = randomFail1 + 1
			end
		end
	else
		randomFail1 = math.random(1,7);
		if math.random(0,1) == 0 then
			randomFail2 = math.random(1,7);
			while randomFail2 ==randomFail1 do
				randomFail2 = math.random(1,7);
			end
		else
			if math.random(0,1) == 0 then
				randomFail2 = randomFail1 - 1
			else
				randomFail2 = randomFail1 + 1
			end
		end
	end
	--print(randomFail1) 
	--print(randomFail2)
	end
		local enemy
		if xFocused then
				if currentSpawnNumber ~= randomFail1 and currentSpawnNumber ~= randomFail2 then
					myXPos = 30+ currentSpawnNumber* 120;
					myYPos = yPos + math.random(-30,30);
					--Instaniate
					
					enemy = display.newImage(randImage)
					allEnemies[#allEnemies + 1]  = enemy
					enemy.x = myXPos ; enemy.y = myYPos
					enemy.rotation = angle
					physics.addBody( enemy, { density=1.0, friction=1, bounce=0 , filter= flt})
					enemy:setLinearVelocity( xVel, yVel )
					enemy.myName = "enemy"
					enemy.isBullet = true
					--Rotate angle 
				end 
				currentSpawnNumber = currentSpawnNumber + 1;
				if currentSpawnNumber > 6 then
					newSpawn = true
				end
			
		else
			if currentSpawnNumber ~= randomFail1 and currentSpawnNumber ~= randomFail2 then
					myXPos = xPos + math.random(-30,30);
					myYPos =  30+ currentSpawnNumber* 120; --
					--Instaniate
					
					enemy = display.newImage(randImage)
					allEnemies[#allEnemies + 1]  = enemy
					enemy.x = myXPos ; enemy.y = myYPos
					enemy.rotation = angle
					physics.addBody( enemy, { density=1.0, friction=1, bounce=0 , filter= flt})
					enemy:setLinearVelocity( xVel, yVel )
					enemy.myName = "enemy"
					enemy.isBullet = true
					--Rotate angle 
				end 
				currentSpawnNumber = currentSpawnNumber + 1;
				if currentSpawnNumber > 8 then
					newSpawn = true
				end
				
			
		end
	
		--print(newSpawn)
		--print(ongoing)
		if newSpawn ==true then
			ongoing = true
			timer.performWithDelay(math.random(450,1050),undoOngoing)
			currentSpawnNumber = 0
			end
end


local function updateScore()
	--print("here")
	for i=1, #allPlayers do
		--print(allPlayers[i].life)
		if allPlayers[i].life>0 then
			allPlayers[i].score.text = allPlayers[i].life
		else
			allPlayers[i].score.text = "DEAD"
		end
	end
end

-- function onTilt( event )
    -- deltaDegreesXText.text =  (9.8*event.xGravity)
    -- deltaDegreesYText.text =  (-9.8*event.yGravity)
    -- --physics.setGravity( ( 9.8 * event.xGravity ), ( -9.8 * event.yGravity ) )
-- end

--timer.performWithDelay(math.random(0.45,1.05),enemies, 1)

local function updateLoop(event)

	updateScore()
	if FRANK_BULLETS then
		if playerIn == 4 then 
			if ongoing == false then
				--print("hello	")
				enemies()
			end
		end
	else
		spawnVerticalEnemies()
		spawnHorizontalEnemies()
		removeOffscreenDilara()
	end
end

local randomGame = 1
local patternedGame = 2


function splash()
	splashGroup = display.newGroup()
	
	local splashBG = display.newImage("splash.png", 0, 0, true)
	--splashBG.rotation = 270
	splashGroup:insert(splashBG)
	
	local randomGameButton = display.newImage( "randm.png", 550, 300, true)
	randomGameButton.id = randomGame
	splashGroup:insert(randomGameButton)

	local patternedGameButton = display.newImage( "pttrnd.png", 550, 600, true)
	patternedGameButton.id = patternedGame
	splashGroup:insert(patternedGameButton)

	randomGameButton:addEventListener("touch", init)
	patternedGameButton:addEventListener("touch", init)
end

function gameStage()
	stageGroup = display.newGroup()
	
	local bkg = display.newRect(0,0,display.contentWidth*2, display.contentHeight*2)
	bkg:setFillColor( 28/255, 34/255, 36/255)
	stageGroup:insert(bkg)

	-- PLAYER SCORE BACKGROUNDS
	local p1_scorebkg = display.newCircle(0,0,50)
	p1_scorebkg:setFillColor(24/255, 101/255, 116/255) -- BLUE
	stageGroup:insert(p1_scorebkg)

	local p2_scorebkg = display.newCircle(display.contentWidth,0,50)
	p2_scorebkg:setFillColor( 51/255, 117/255, 99/255 ) -- GREEN
	stageGroup:insert(p2_scorebkg)
	
	local p3_scorebkg = display.newCircle(0,display.contentHeight,50)
	p3_scorebkg:setFillColor( 107/255, 70/255, 117/255 ) -- PURPLE
	stageGroup:insert(p3_scorebkg)

	local p4_scorebkg = display.newCircle(display.contentWidth,display.contentHeight,50)
	p4_scorebkg:setFillColor(162/255, 59/255, 59/255) -- RED
	stageGroup:insert(p4_scorebkg)
	-- DONE PLAYER SCORE BACKGROUNDS
	
	
	stageGroup:insert(purpleScore)
	stageGroup:insert(greenScore)
	stageGroup:insert(blueScore)
	stageGroup:insert(redScore)

	initializePlayers()
	
	Runtime:addEventListener("enterFrame", updateLoop)
	Runtime:addEventListener( "collision", onCollision )
	
end


function init(event)
	mode = event.target.id
	
	if mode == randomGame then
		splashGroup:removeSelf()-- Removes splash screen objects
		FRANK_BULLETS = false
		timer.performWithDelay(200, gameStage, 1)
	elseif mode == patternedGame then
		splashGroup:removeSelf()-- Removes splash screen objects
		FRANK_BULLETS = true
		timer.performWithDelay(200, gameStage, 1)
	end
end

splash()

-- Add gyroscope listeners, but only if the device has a gyroscope.
if system.hasEventSource("gyroscope") then
	Runtime:addEventListener("gyroscope", onTilt)
end

