-------------------------------------------------------------------------------------

display.setStatusBar( display.HiddenStatusBar )

local physics = require("physics")
local gameUI = require("gameUI")
local easingx = require("easingx")

local blueLife = 5
local redLife = 5
local purpleLife = 5
local greenLife = 5

local blueX = 64
local blueY = 64

local redX = display.contentWidth - 64
local redY = display.contentHeight - 64

local purpleX = 64
local purpleY = display.contentHeight - 64

local greenX = display.contentWidth - 64
local greenY = 64

local blueCollisionFilter   = { categoryBits = 1, maskBits = 224 } --16
local greenCollisionFilter  = { categoryBits = 2, maskBits = 208 } --32
local redCollisionFilter    = { categoryBits = 4, maskBits = 176 } --64
local purpleCollisionFilter = { categoryBits = 8, maskBits = 112} --128

local blueEnemyFilter   = { categoryBits = 16, maskBits = 14 } --1
local greenEnemyFilter  = { categoryBits = 32, maskBits = 13 } --2
local redEnemyFilter    = { categoryBits = 64, maskBits = 11 } --4
local purpleEnemyFilter = { categoryBits = 128, maskBits = 7}--8


physics.start()
physics.setGravity( 0, 0 ) -- no gravity in any direction

popSound = audio.loadSound ("pop2_wav.wav")
labelFont = gameUI.newFontXP{ ios="Zapfino", android=native.systemFont }

system.activate( "multitouch" )

local bkg = display.newRect(0,0,display.contentWidth*2, display.contentHeight*2)
bkg:setFillColor( 28/255, 34/255, 36/255)

local deltaDegreesXText = display.newText("gyrooooo",200,500, nil, 50)

local blueScore = display.newText(blueLife, 64, 32, nil, 50)
blueScore.xScale = 1.2
blueScore.yScale = 1.2
blueScore.rotation = 90

local redScore = display.newText(redLife, display.contentWidth-64, display.contentHeight-100, nil, 50)
redScore.xScale = 1.2
redScore.yScale = 1.2
redScore.rotation = 270

local greenScore = display.newText(greenLife, display.contentWidth-64, 32, nil, 50)
greenScore.xScale = 1.2
greenScore.yScale = 1.2
greenScore.rotation = 270

local purpleScore = display.newText(purpleLife, 64, display.contentHeight-100, nil, 50)
purpleScore.xScale = 1.2
purpleScore.yScale = 1.2
purpleScore.rotation = 90


local enemyGfx = { "rocket_blue.png", "rocket_green.png", "rocket_red.png", "rocket_purple.png" }
local allVerticalEnemies = {} -- empty table for storing objects
local allHorizontalEnemies = {}
-- Automatic culling of offscreen objects
local function removeOffscreenItems()
	for i = 1, #allVerticalEnemies do
		local oneEnemy = allVerticalEnemies[i]
		if (oneEnemy and oneEnemy.x) then
			if oneEnemy.x < -100 or oneEnemy.x > display.contentWidth + 100 or oneEnemy.y < -100 or oneEnemy.y > display.contentHeight + 100 then
				oneEnemy:removeSelf()
                table.remove( allVerticalEnemies, i ) 
 			end	
		end
	end
	
	for i = 1, #allHorizontalEnemies do
		local oneEnemy = allHorizontalEnemies[i]
		if (oneEnemy and oneEnemy.x) then
			if oneEnemy.x < -100 or oneEnemy.x > display.contentWidth + 100 or oneEnemy.y < -100 or oneEnemy.y > display.contentHeight + 100 then
				oneEnemy:removeSelf()
                table.remove( allHorizontalEnemies, i ) 
 			end	
		end
	end
end


local function spawnVerticalEnemies()
--Choose colors 

--randomly select X position between 0 to contentwidth

	num = math.random(0, 60)
	if (num % 17 == 1) then  

		enemyRand = math.random(1,4)
		if enemyRand == 1 then
			flt = blueEnemyFilter	
		elseif enemyRand == 2 then
			flt = greenEnemyFilter
		elseif enemyRand == 3 then
			flt = redEnemyFilter
		else
			flt = purpleEnemyFilter
		
		end
		
		randImage = enemyGfx[ enemyRand ]

		allVerticalEnemies[#allVerticalEnemies + 1] = display.newImage( randImage )
		local enemy = allVerticalEnemies[#allVerticalEnemies]
		
		enemy.x = math.random(140,display.contentWidth-140)
		enemy.y = 0 --math.random(0,display.contentHeight)
		enemy.rotation = 90
		--RANDOM X (0-contentwidth)
		--RANDOM Y ( minus values)
		physics.addBody( enemy, { density=1.0, friction=1, bounce=0 , filter= flt})
		enemy:setLinearVelocity( 0, 100 )
		enemy.myName = "enemy"
				enemy.isBullet = true

	end
end

local function spawnHorizontalEnemies()
--Choose colors 

--randomly select Y position between 0 to contentheight
	num = math.random(0, 60)
	if (num % 17 == 1) then  

		enemyRand = math.random(1,4)
		if enemyRand == 1 then
			flt = blueEnemyFilter	
		elseif enemyRand == 2 then
			flt = greenEnemyFilter
		elseif enemyRand == 3 then
			flt = redEnemyFilter
		else
			flt = purpleEnemyFilter
		
		end
		
		randImage = enemyGfx[ enemyRand ]
		
		allHorizontalEnemies[#allHorizontalEnemies + 1] = display.newImage( randImage )
		local enemy = allHorizontalEnemies[#allHorizontalEnemies]
		
		enemy.y = math.random(140,display.contentHeight-140)
		enemy.x = 0 --math.random(0,display.contentHeight)
	
		physics.addBody( enemy, { density=1.0, friction=1, bounce=0 , filter= flt})
		enemy:setLinearVelocity( 100, 0 )
		enemy.myName = "enemy"
		enemy.isBullet = true

	end

end



local function startDrag( event )
	local t = event.target
	local phase = event.phase
	local stage = display.getCurrentStage()
	
	local phase = event.phase
	if "began" == phase then
		stage:setFocus( t, event.id )
		t.isFocus = true

		-- Store initial position
		t.x0 = event.x - t.x
		t.y0 = event.y - t.y
		
		-- Make body type temporarily "kinematic" (to avoid gravitional forces)
		--event.target.bodyType = "kinematic"
		
		-- Stop current motion, if any
		t:setLinearVelocity( 0, 0 )
		t.angularVelocity = 0

	elseif t.isFocus then
		
			if "moved" == phase then
				t.x = event.x - t.x0
				t.y = event.y - t.y0

			elseif "ended" == phase or "cancelled" == phase then
				stage:setFocus( nil )
				t.isFocus = false
				-- Stop current motion, if any
				t:setLinearVelocity( 0, 0 )
				t.angularVelocity = 0
				
			end
	end

	-- Stop further propagation of touch event!
	return true
end

local function initializePlayers()

	local blue_player = display.newImage("fingermark_blue.png", 128,128);
	blue_player.x = blueX
	blue_player.y = blueY
	blue_player.life = blueLife
	physics.addBody(blue_player,{ density=1.0, friction=1, bounce=0, filter = blueCollisionFilter} )
	blue_player:addEventListener( "touch", startDrag ) -- make object draggable
	
	local purple_player = display.newImage("fingermark_purple.png", 128,128);
	purple_player.x = purpleX
	purple_player.y = purpleY
	purple_player.life = purpleLife
	
	local green_player = display.newImage("fingermark_green.png", 128,128);
	green_player.x = greenX
	green_player.y = greenY
	green_player.life = greenLife
	
	local red_player = display.newImage("fingermark_red.png", 128,128);
	red_player.x = redX
	red_player.y = redY
	red_player.life = redLife
	
	physics.addBody(red_player,{ density=1.0, friction=1, bounce=0, filter =redCollisionFilter } )
	physics.addBody(purple_player,{ density=1.0, friction=1, bounce=0, filter =purpleCollisionFilter } )
	physics.addBody(green_player,{ density=1.0, friction=1, bounce=0, filter =greenCollisionFilter } )
	
	red_player.bodyType = "kinematic"
	blue_player.bodyType = "kinematic"
	purple_player.bodyType = "kinematic"
	green_player.bodyType = "kinematic"
	
	purple_player:addEventListener( "touch", startDrag ) -- make object draggable
	red_player:addEventListener( "touch", startDrag ) -- make object draggable
	green_player:addEventListener( "touch", startDrag ) -- make object draggable
	
	green_player.myName = "green_player"	
	blue_player.myName = "blue_player"
	purple_player.myName = "purple_player"
	red_player.myName = "red_player"

end

local function initializePurple()

	local blue_player = display.newImage("fingermark_blue.png", 128,128);
	blue_player.x = 300;
	blue_player.y = 300;
	
	physics.addBody(blue_player,{ density=1.0, friction=1, bounce=0 } )
	blue_player:addEventListener( "touch", startDrag ) -- make object draggable
end

local function initializeRed()

	local blue_player = display.newImage("fingermark_blue.png", 128,128);
	blue_player.x = 300;
	blue_player.y = 300;
	
	physics.addBody(blue_player,{ density=1.0, friction=1, bounce=0 } )
	blue_player:addEventListener( "touch", startDrag ) -- make object draggable
end

local function spawnDisk( event )
	local phase = event.phase
	
	if "ended" == phase then
		audio.play( popSound )
		myLabel.isVisible = false

		randImage = diskGfx[ math.random( 1, 3 ) ]
		allDisks[#allDisks + 1] = display.newImage( randImage )
		local disk = allDisks[#allDisks]
		disk.x = event.x; disk.y = event.y
		disk.rotation = math.random( 1, 360 )
		disk.xScale = 0.8; disk.yScale = 0.8
		
		transition.to(disk, { time = 500, xScale = 1.0, yScale = 1.0, transition = easingx.easeOutElastic }) -- "pop" animation
		
		--local platform = display.newImage( "platform.png", 600, 200 )
		physics.addBody( disk, { density=1.0, friction=1, bounce=0 } )
		--physics.addBody( disk, { density=0.3, friction=1, radius=66.0 } )
		--disk.linearDamping = 0.4
		--disk.angularDamping = 0.6
		
		disk:addEventListener( "touch", startDrag ) -- make object draggable
	end
	
	return true
end

initializePlayers()


local function onCollision( event )

   if ( event.phase == "began" ) then

     -- print( "began: " .. event.object1.myName .. " and " .. event.object2.myName )
	  
			  -- Collision event
		local function delay()
			-- Change the body's type to static
			if event.object1.life >0 then 
				event.object1.life = event.object1.life -1
			end
			if event.object1.myName == "blue_player" then
				--event.object1.x = blueX
				--event.object1.y = blueY
				blueLife = blueLife - 1
			elseif event.object1.myName == "red_player" then
				--event.object1.x = redX
				--event.object1.y = redY
				redLife = redLife -1
			elseif event.object1.myName == "green_player" then
				--event.object1.x = greenX
				--event.object1.y = greenY
				greenLife = greenLife - 1
			elseif event.object1.myName == "purple_player" then
				--event.object1.x = purpleX
				--event.object1.y = purpleY
				purpleLife = purpleLife - 1
			end
			
			local explode = display.newImage("bang.png")
			explode.x = event.object2.x; explode.y = event.object2.y
			explode.xScale = 0.8; explode.yScale = 0.8
		
			transition.to(explode, { time = 500, xScale = 1.5, yScale = 1.5, transition = easingx.easeOut }) -- "pop" animation
			transition.to( explode, { time=500, delay=500, alpha=0.0 })
			
			--event.object2:removeSelf()
			--event.object2 = nil
			
			event.object2.x = -100
			event.object2.y = -100
			
			print (event.object1.myName .. " " .. event.object1.life)
			
			if event.object1.life <= 0 then
				event.object1.x = -100
				event.object1.y = -100
				--if event.object1 ~= nil then 
					--event.object1.isFocus = false
					--print(event.object1)
						--event.object1:removeSelf()
				--end
			
			end
			
		end

		timer.performWithDelay( 100, delay )

	end
end


local function enemies(event)

	spawnVerticalEnemies()
	spawnHorizontalEnemies()
	removeOffscreenItems()
end

local function updateScore()
	if blueLife >= 0 then
		blueScore.text = blueLife
	else
		blueScore.text = "DEAD"
	end
	if redLife >= 0 then
		redScore.text = redLife
	else
		redScore.text = "DEAD"
	end
	if greenLife >= 0 then
		greenScore.text = greenLife
	else
		greenScore.text = "DEAD"
	end
	if purpleLife >= 0 then
		purpleScore.text = purpleLife
	else
		purpleScore.text = "DEAD"
	end
end

timer.performWithDelay(1, updateScore, -1)

function onTilt( event )
    deltaDegreesXText.text =  (9.8*event.xGravity)
    deltaDegreesYText.text =  (-9.8*event.yGravity)
    --physics.setGravity( ( 9.8 * event.xGravity ), ( -9.8 * event.yGravity ) )
end


--bkg:addEventListener( "touch", spawnDisk ) -- touch the screen to create disks
Runtime:addEventListener( "enterFrame", enemies ) -- clean up offscreen disks
Runtime:addEventListener( "collision", onCollision )
-- Add gyroscope listeners, but only if the device has a gyroscope.
if system.hasEventSource("gyroscope") then
	Runtime:addEventListener("gyroscope", onTilt)
end

